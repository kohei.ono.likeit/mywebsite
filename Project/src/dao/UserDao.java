package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.User;


public class UserDao {
	//ログイン時に使用するメソッド
	public User findByLoginInfo(String loginId, String password, String id, String name, String birthDate) {
	    Connection conn = null;
	        try {
	            // データベースへ接続
				  conn = DBManager.getConnection();

				  String sql="SELECT * FROM user WHERE login_id=? and password=?";
				  PreparedStatement pStmt = conn.prepareStatement(sql);
				  pStmt.setString(1,loginId);
				  pStmt.setString(2,password);
				  ResultSet rs = pStmt.executeQuery();

			//	  ログイン失敗時 1件も結果がない為、nullを返す
				  if(!rs.next()) {
					  return null;
				  }

			//	  ログイン成功時 ResultSetからカラム名を指定してレコード内のデータを取り出し、
			//	  Beansインスタンスのフィールドにセットして返す
				  int userId=rs.getInt("id");
				  String loginIdDate=rs.getString("login_id");
				  String nameDate=rs.getString("name");
				  String passwordDate=rs.getString("password");
				  String userBirthDate=rs.getString("birth_date");
				  return new User(userId,loginIdDate,nameDate,passwordDate,userBirthDate);

	        } catch (SQLException e) {
	        	e.printStackTrace();
	        } finally {
	        	// データベース切断
	          if (conn != null) {
	          	try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	        return null;
	}

	//	ユーザー登録時に使用するメソッド
	public User UserFormDao(String loginId, String password, String passwordCheck, String name, String birthDate) {
		//		UserFormServletに戻す
		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			String sql = "INSERT INTO user(login_id,password,name,birth_date) VALUES (?,?,?,?)";
			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			pStmt.setString(3, name);
			pStmt.setString(4, birthDate);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return null;
	}


	//	ユーザー登録時のエラー表示に使用するメソッド
	public User UserFormIdCheck(String loginId) {
		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			String sql = "select * from user where login_id=?";
			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			String loginIdDate = rs.getString("login_id");
			return new User(loginIdDate);

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return null;
	}


	//	ユーザー情報修正、更新メソッド
	public User UpdateUserInfo(String password, String passwordCheck, String name, String id) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "UPDATE user SET password=?,name=? WHERE id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1,password);
			pStmt.setString(2,name);
			pStmt.setString(3,id);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}


	//	ログイン時のセッション情報の更新
	public User LoginInfoUpdate(String id) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "select * from user where id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1,id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int UserId = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			String password = rs.getString("password");
			String birthDate = rs.getString("birth_date");

			return new User(UserId,loginId,name,password,birthDate);


		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	//	ユーザー情報を表示するメソッド
//	public User UserInfo(String id) {
//		Connection conn = null;
//		try {
//			conn = DBManager.getConnection();
//			String sql = "SELECT * FROM user WHERE id=?";
//
//			PreparedStatement pStmt = conn.prepareStatement(sql);
//			pStmt.setString(1,id);
//			ResultSet rs = pStmt.executeQuery();
//
//			if (!rs.next()) {
//				return null;
//			}
//
//			int userId = rs.getInt("id");
//			String loginId = rs.getString("login_id");
//			String password = rs.getString("password");
//			String name = rs.getString("name");
//			String birthDate = rs.getString("birth_date");
//
//			return new User(userId,loginId,password,name,birthDate);
//
//
//		} catch (SQLException e) {
//			e.printStackTrace();
//
//		} finally {
//			// データベース切断
//			if (conn != null) {
//				try {
//					conn.close();
//				} catch (SQLException e) {
//					e.printStackTrace();
//				}
//			}
//		}
//		return null;
//	}
}