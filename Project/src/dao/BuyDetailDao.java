package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.BuyDetail;
import model.Item;

public class BuyDetailDao {
	//	購入詳細情報登録
	public static void insertBuyDetail(BuyDetail bd) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO buy_detail(buy_id,item_id) VALUES(?,?)");
			st.setInt(1, bd.getBuyId());
			st.setInt(2, bd.getItemId());
			st.executeUpdate();

		} catch (SQLException e) {
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//	購入idをもとに購入詳細情報検索
	public static ArrayList<Item> getItemDataListByBuyId(int buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT item.id,"
					+ " item.name,"
					+ " item.price,"
					+ " item.file_name"
					+ " FROM buy_detail"
					+ " JOIN item"
					+ " ON buy_detail.item_id = item.id"
					+ " WHERE buy_detail.buy_id = ?");
			st.setInt(1, buyId);

			ResultSet rs = st.executeQuery();
			ArrayList<Item> buyDetailItemList = new ArrayList<Item>();

			while (rs.next()) {
				Item item = new Item();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setPrice(rs.getInt("price"));
				item.setFileName(rs.getString("file_name"));

				buyDetailItemList.add(item);
			}

			return buyDetailItemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
				}
			}
		}

	// 購入履歴詳細
	public static ArrayList<Item> getItemDataListByBuyId(String buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		ArrayList<Item> buyDetailItemList = new ArrayList<Item>();
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT item.id,"
					+ " item.name,"
					+ " item.price,"
					+ " item.file_name"
					+ " FROM buy_detail"
					+ " JOIN item"
					+ " ON buy_detail.item_id = item.id"
					+ " WHERE buy_detail.buy_id = ?");

			st.setString(1, buyId);

			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				Item item = new Item();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setPrice(rs.getInt("price"));
				item.setFileName(rs.getString("file_name"));

				buyDetailItemList.add(item);
			}

		} catch (SQLException e) {
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
		return buyDetailItemList;
}

}
