package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.DeliveryMethod;

public class DeliveryMethodDao {

	//	配送方法を指定
	public static ArrayList<DeliveryMethod> getAllDeliveryMethod() throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM delivery_method");

			ResultSet rs = st.executeQuery();

			ArrayList<DeliveryMethod> deliveryMethodList = new ArrayList<DeliveryMethod>();
			while (rs.next()) {
				DeliveryMethod dml = new DeliveryMethod();
				dml.setId(rs.getInt("id"));
				dml.setName(rs.getString("name"));
				dml.setPrice(rs.getInt("price"));
				deliveryMethodList.add(dml);
			}

			return deliveryMethodList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//	deliveryMethodのidを元に配送方法を取得
	public static DeliveryMethod getDeliveryMethodByID(int DeliveryMethodId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM delivery_method WHERE id = ?");
			st.setInt(1, DeliveryMethodId);

			ResultSet rs = st.executeQuery();

			DeliveryMethod dm = new DeliveryMethod();
			if (rs.next()) {
				dm.setId(rs.getInt("id"));
				dm.setName(rs.getString("name"));
				dm.setPrice(rs.getInt("price"));
			}

			return dm;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

}
