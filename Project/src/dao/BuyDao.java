package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import model.Buy;

public class BuyDao {

	// 購入情報登録
	public static int insertBuy(Buy IB) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		int autoIncKey = -1;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO buy(user_id,total_price,delivery_method_id,buy_date) VALUES(?,?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			st.setInt(1, IB.getUserId());
			st.setInt(2, IB.getTotalPrice());
			st.setInt(3, IB.getDeliveryMethodId());
			st.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
			st.executeUpdate();

			ResultSet rs = st.getGeneratedKeys();
			if (rs.next()) {
				autoIncKey = rs.getInt(1);
			}

			return autoIncKey;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	// buyIdをもとに購入情報検索
	public static Buy getBuyDataByBuyId(int buyId) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM buy" + " JOIN delivery_method"
					+ " ON buy.delivery_method_id = delivery_method.id" + " WHERE buy.id = ?");
			st.setInt(1, buyId);

			ResultSet rs = st.executeQuery();

			Buy buy = new Buy();
			if (rs.next()) {
				buy.setId(rs.getInt("id"));
				buy.setTotalPrice(rs.getInt("total_price"));
				buy.setBuyDate(rs.getTimestamp("buy_date"));
				buy.setDeliveryMethodId(rs.getInt("delivery_method_id"));
				buy.setUserId(rs.getInt("user_id"));
				buy.setDeliveryMethodPrice(rs.getInt("price"));
				buy.setDeliveryMethodName(rs.getString("name"));
			}

			return buy;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	// ユーザー購入履歴
	public static List<Buy> getBuyDateBuyId(int buyId) throws SQLException {
		Connection con = null;
		List<Buy> userBuyList = new ArrayList<Buy>();

		try {
			con = DBManager.getConnection();

			String sql = "SELECT * FROM buy" + " JOIN delivery_method"
					+ " ON buy.delivery_method_id = delivery_method.id" + " WHERE buy.user_id = ?";

			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setInt(1, buyId);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				Buy buy = new Buy();
				buy.setId(rs.getInt("id"));
				buy.setTotalPrice(rs.getInt("total_price"));
				buy.setBuyDate(rs.getTimestamp("buy_date"));
				buy.setDeliveryMethodId(rs.getInt("delivery_method_id"));
				buy.setUserId(rs.getInt("user_id"));
				buy.setDeliveryMethodPrice(rs.getInt("price"));
				buy.setDeliveryMethodName(rs.getString("name"));

				userBuyList.add(buy);
			}

		} catch (SQLException e) {
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
		return userBuyList;
	}


	//	購入履歴詳細　String
	public static Buy getBuyHistoryByBuyId(String buyId) throws SQLException {
		Connection con = null;

		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM buy" + " JOIN delivery_method"
					+ " ON buy.delivery_method_id = delivery_method.id" + " WHERE buy.id = ?");
			st.setString(1, buyId);

			ResultSet rs = st.executeQuery();

			Buy buy = new Buy();
			if (rs.next()) {
				buy.setId(rs.getInt("id"));
				buy.setTotalPrice(rs.getInt("total_price"));
				buy.setBuyDate(rs.getTimestamp("buy_date"));
				buy.setDeliveryMethodId(rs.getInt("delivery_method_id"));
				buy.setUserId(rs.getInt("user_id"));
				buy.setDeliveryMethodPrice(rs.getInt("price"));
				buy.setDeliveryMethodName(rs.getString("name"));
			}

			return buy;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
}
