package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import model.Favorite;

public class FavoriteDao {
	public Favorite favoriteAdd(String itemId, String userId) {
	    Connection conn = null;
	        try {
	            // データベースへ接続
	  conn = DBManager.getConnection();

	  String sql = "INSERT INTO favorite_list (item_id,user_id) VALUES (?,?)";
	  PreparedStatement pStmt = conn.prepareStatement(sql);

	  pStmt.setString(1, itemId);
	  pStmt.setString(2, userId);

	  pStmt.executeUpdate();

	} catch (SQLException e) {
	        	e.printStackTrace();
	} finally {
	 // データベース切断
	          if (conn != null) {
	          	try {
	                		conn.close();
	            	} catch (SQLException e) {
	                		e.printStackTrace();
	            	}
	        	}
	}
	return null;
	}
}
