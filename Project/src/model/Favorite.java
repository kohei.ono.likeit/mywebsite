package model;

public class Favorite {
	private int id;
	private int itemId;
	private int userId;


	//	お気に入り追加
	public Favorite(int itemId, int userId) {
		this.itemId=itemId;
		this.userId=userId;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Favorite() {
	}

}
