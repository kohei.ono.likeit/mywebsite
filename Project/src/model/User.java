package model;

import java.io.Serializable;

public class User implements Serializable{
	private int id;
	private String loginId;
	private String password;
	private String name;
	private String birthDate;


	public User() {
	}


	// ログイン
	public User(int id, String loginId, String name, String password, String birthDate) {
		this.id=id;
		this.loginId = loginId;
		this.name = name;
		this.password= password;
		this.birthDate= birthDate;
	}

	//	ユーザー登録
	public User(String loginId,String password,String name, String birthDate) {
		this.loginId = loginId;
		this.password = password;
		this.name = name;
		this.birthDate = birthDate;
	}

	//	ユーザー登録時のエラー表示
	public User(String loginIdDate) {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	//	ユーザー情報修正
	public User(String password, String name, int id) {
		this.password = password;
		this.name = name;
		this.id = id;
	}

	//	ユーザー情報を表示
//	public User(int userId, String loginId, String password, String name, String birthDate) {
//		// TODO 自動生成されたコンストラクター・スタブ
//	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}


//	ユーザー更新
	public void setUpdateUserInfo(String password,String loginId) {
		this.password = password;
		this.loginId = loginId;
	}


}
