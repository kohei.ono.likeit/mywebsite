package model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Buy implements Serializable {
	private int id;
	private int userId;
	private int totalPrice;
	private int deliveryMethodId;
	private String deliveryMethodName;
	private int deliveryMethodPrice;
	private Date buyDate;


	public Buy() {
	}

	public Buy(int id, int userId, int totalPrice, int deliveryMethodId, String deliveryMethodName,
			int deliveryMethodPrice) {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}

	public int getDeliveryMethodId() {
		return deliveryMethodId;
	}

	public void setDeliveryMethodId(int deliveryMethodId) {
		this.deliveryMethodId = deliveryMethodId;
	}

	public String getDeliveryMethodName() {
		return deliveryMethodName;
	}

	public void setDeliveryMethodName(String deliveryMethodName) {
		this.deliveryMethodName = deliveryMethodName;
	}

	public int getDeliveryMethodPrice() {
		return deliveryMethodPrice;
	}

	public void setDeliveryMethodPrice(int deliveryMethodPrice) {
		this.deliveryMethodPrice = deliveryMethodPrice;
	}

	public Date getBuyDate() {
		return buyDate;
	}

	public void setBuyDate(Date buyDate) {
		this.buyDate = buyDate;
	}

	public String getFormatDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd (E)");
		return sdf.format(buyDate);
	}

	public String getFormatTotalPrice() {
		return String.format("%,d", this.totalPrice);
	}

	//	合計金額を求める
	public static int getTotalItemPrice(ArrayList<Item> items) {
		int total = 0;
		for (Item item : items) {
			total += item.getPrice();
		}
		return total;
	}
}
