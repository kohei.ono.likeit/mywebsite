package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ItemDao;
import model.Item;
import model.User;

/**
 * Servlet implementation class CartAddFinishServlet
 */
@WebServlet("/CartAddFinishServlet")
public class CartAddFinishServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CartAddFinishServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// セッションタイムアウト
		HttpSession session = request.getSession(false);
		User u=(User)session.getAttribute("user");

		if (u == null){
		response.sendRedirect("LoginServlet");
		return;
		}

		//	id取得
		String id = request.getParameter("item_id");
		System.out.println(id);


		//	対象のアイテム情報を取得
		ItemDao itemDao = new ItemDao();
		Item item = itemDao.ItemDetail(id);
		request.setAttribute("item", item);

		//カートを取得
		ArrayList<Item> cart = (ArrayList<Item>) session.getAttribute("cart");

		//セッションにカートがない場合カートを作成
		if (cart == null) {
			cart = new ArrayList<Item>();
		}

		//カートに商品を追加
		cart.add(item);
		session.setAttribute("cart", cart);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/cart_add_finish.jsp");
		dispatcher.forward(request,response);
	}

}
