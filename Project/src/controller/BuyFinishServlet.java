package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BuyDao;
import dao.BuyDetailDao;
import model.Buy;
import model.BuyDetail;
import model.Item;
import model.User;

/**
 * Servlet implementation class BuyFinishServlet
 */
@WebServlet("/BuyFinishServlet")
public class BuyFinishServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BuyFinishServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッションタイムアウト
		HttpSession session = request.getSession(false);
		User u = (User) session.getAttribute("user");

		if (u == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		try {
			// カート情報取得
			ArrayList<Item> cartList = (ArrayList<Item>) session.getAttribute("cart");

			// セッションをキャストして呼び出し
			Buy buy = (Buy) session.getAttribute("buy");

			// 購入情報登録
			int buyId = BuyDao.insertBuy(buy);

			// 購入詳細情報を購入情報IDに紐づけして登録
			for (Item cartInItem : cartList) {
				BuyDetail bd = new BuyDetail();
				bd.setBuyId(buyId);
				bd.setItemId(cartInItem.getId());
				BuyDetailDao.insertBuyDetail(bd);
			}

			//	購入idをもとに購入情報の表示
			Buy buyResult = BuyDao.getBuyDataByBuyId(buyId);
			request.setAttribute("buyResult", buyResult);

			//	購入した商品情報
			ArrayList<Item> buyResultList = BuyDetailDao.getItemDataListByBuyId(buyId);
			request.setAttribute("buyResultList", buyResultList);

			// カート内のセッション情報を削除
			session.removeAttribute("cart");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buy_finish.jsp");
			dispatcher.forward(request, response);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
