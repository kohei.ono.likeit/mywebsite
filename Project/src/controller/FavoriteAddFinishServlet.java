package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.FavoriteDao;
import dao.ItemDao;
import model.Favorite;
import model.Item;
import model.User;

/**
 * Servlet implementation class FavoriteAddFinishServlet
 */
@WebServlet("/FavoriteAddFinishServlet")
public class FavoriteAddFinishServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FavoriteAddFinishServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッションタイムアウト
		HttpSession session = request.getSession(false);
		User u = (User) session.getAttribute("user");

		if (u == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		String itemId = request.getParameter("itemId");
		String userId = request.getParameter("userId");

		FavoriteDao favoriteDao = new FavoriteDao();
		Favorite fAdd = favoriteDao.favoriteAdd(itemId, userId);

		request.setAttribute("fAdd", fAdd);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/favorite_add_finish.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッションタイムアウト
		HttpSession session = request.getSession(false);
		User u = (User) session.getAttribute("user");

		if (u == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		// id取得
		String id = request.getParameter("item_id");
		System.out.println(id);

		// 対象のアイテム情報を取得
		ItemDao itemDao = new ItemDao();
		Item item = itemDao.ItemDetail(id);
		request.setAttribute("item", item);

		// カートを取得
		ArrayList<Item> favorite = (ArrayList<Item>) session.getAttribute("favorite");

		// セッションにカートがない場合カートを作成
		if (favorite == null) {
			favorite = new ArrayList<Item>();
		}

		// カートに商品を追加。
		favorite.add(item);
		session.setAttribute("favorite", favorite);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/favorite_add_finish.jsp");
		dispatcher.forward(request, response);
	}
}
