package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ItemDao;
import model.Item;
import model.User;

/**
 * Servlet implementation class ItemDetailServlet
 */
@WebServlet("/ItemDetailServlet")
public class ItemDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// セッションタイムアウト
		HttpSession session = request.getSession(false);
		User u=(User)session.getAttribute("user");

		if (u == null){
		response.sendRedirect("LoginServlet");
		return;

		}

		//	idを元にアイテム詳細を表示
		String id = request.getParameter("item_id");
		System.out.println(id);

		ItemDao itemDao = new ItemDao();
		Item item = itemDao.ItemDetail(id);

		request.setAttribute("item", item);

		//閲覧履歴を取得
		ArrayList<Item> searchHistory = (ArrayList<Item>) session.getAttribute("searchHistory");

		//セッションに閲覧履歴がない場合閲覧履歴を作成
		if (searchHistory == null) {
			searchHistory = new ArrayList<Item>();
		}

		//閲覧履歴に商品を追加
		searchHistory.add(item);
		session.setAttribute("searchHistory", searchHistory);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/item_detail.jsp");
		dispatcher.forward(request,response);
	}
}
