package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BuyDao;
import model.Buy;
import model.User;

/**
 * Servlet implementation class BuyHistoryServlet
 */
@WebServlet("/BuyHistoryServlet")
public class BuyHistoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BuyHistoryServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			// セッションタイムアウト
			HttpSession session = request.getSession(false);
			User u = (User) session.getAttribute("user");

			if (u == null) {
				response.sendRedirect("LoginServlet");
				return;
			}

			User user = (User) session.getAttribute("user");

			List<Buy> buyHistory = BuyDao.getBuyDateBuyId(user.getId());
			request.setAttribute("buyHistory", buyHistory);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buy_history.jsp");
			dispatcher.forward(request, response);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
