package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ItemDao;
import model.Item;
import model.User;

/**
 * Servlet implementation class ItemListServlet
 */
@WebServlet("/ItemListServlet")
public class ItemListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		try {
			// セッションタイムアウト
			HttpSession session = request.getSession(false);
			User u=(User)session.getAttribute("user");

			if (u == null){
			response.sendRedirect("LoginServlet");
			return;
			}

			//	検索ワードを取得
			String searchWord = request.getParameter("searchWord");

			//	検索ワードをセッションで残す
			session.setAttribute("searchWord", searchWord);

			//	検索結果の総数表示
			double itemCount = ItemDao.getItemCount(searchWord);
			request.setAttribute("itemCount", (int) itemCount);


			//	検索実行

			ItemDao itemDao = new ItemDao();
			ArrayList<Item> itemList = itemDao.itemSearch(searchWord);

			request.setAttribute("itemList", itemList);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/item_list.jsp");
			dispatcher.forward(request,response);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
