package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Item;
import model.User;

/**
 * Servlet implementation class FavoriteListServlet
 */
@WebServlet("/FavoriteListServlet")
public class FavoriteListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FavoriteListServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// セッションタイムアウト
		HttpSession session = request.getSession(false);
		User u=(User)session.getAttribute("user");

		if (u == null){
		response.sendRedirect("LoginServlet");
		return;
		}

		try {
			ArrayList<Item> favorite = (ArrayList<Item>) session.getAttribute("favorite");
			//セッションにお気に入りがない場合お気に入りを作成
			if (favorite == null) {
				favorite = new ArrayList<Item>();
				session.setAttribute("favorite", favorite);
			}

		String cartActionMessage = "";
		//お気に入りに商品が入っていないなら
		if(favorite.size() == 0) {
			cartActionMessage = "お気に入りに商品がありません";
		}

		request.setAttribute("cartActionMessage", cartActionMessage);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/favorite_list.jsp");
		dispatcher.forward(request,response);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
