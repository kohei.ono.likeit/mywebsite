package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DeliveryMethodDao;
import model.Buy;
import model.DeliveryMethod;
import model.Item;
import model.User;

/**
 * Servlet implementation class BuyCheckServlet
 */
@WebServlet("/BuyCheckServlet")
public class BuyCheckServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
//		 セッションタイムアウト
		 HttpSession session = request.getSession(false);
		 User u = (User) session.getAttribute("user");

		 if (u == null) {
		 response.sendRedirect("LoginServlet");
		 return;
		 }

		try {
			// 選択された配送方法idを取得
			int deliveryMethodId = Integer.parseInt(request.getParameter("delivery_method_id"));
			System.out.println(deliveryMethodId);

			// 選択された配送方法idをもとに配送方法、金額を取得
			DeliveryMethod dm = DeliveryMethodDao.getDeliveryMethodByID(deliveryMethodId);

			// セッションのカートを取得
			ArrayList<Item> cartList = (ArrayList<Item>) session.getAttribute("cart");

			// 合計金額
			int totalPrice = Buy.getTotalItemPrice(cartList);

			Buy buy = new Buy();
			User user = (User) session.getAttribute("user");
			buy.setUserId(user.getId());
			buy.setDeliveryMethodId(dm.getId());
			buy.setDeliveryMethodName(dm.getName());
			buy.setDeliveryMethodPrice(dm.getPrice());

			totalPrice += buy.getDeliveryMethodPrice();
			buy.setTotalPrice(totalPrice);

			session.setAttribute("buy", buy);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buy_check.jsp");
			dispatcher.forward(request, response);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
