package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserInfoFixFinishServlet
 */
@WebServlet("/UserInfoFixFinishServlet")
public class UserInfoFixFinishServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserInfoFixFinishServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 文字化け防止
		request.setCharacterEncoding("UTF-8");

		// セッションタイムアウト
		 HttpSession session = request.getSession(false);
		 User u = (User) session.getAttribute("user");

		 if (u == null) {
		 response.sendRedirect("LoginServlet");
		 return;
		 }

		//入力した内容をゲット
		String password = request.getParameter("password");
		String passwordCheck = request.getParameter("passwordCheck");
		String name = request.getParameter("name");
		String id = request.getParameter("id");

		UserDao userDao =new UserDao();
		User user = userDao.UpdateUserInfo(password, passwordCheck, name, id);

		//	ログイン時のセッション情報の更新
		User userInfo = userDao.LoginInfoUpdate(id);
		session.setAttribute("user",userInfo);
		request.setAttribute("updateUI",user);

		//jspへフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_info_fixFinish.jsp");
		dispatcher.forward(request,response);
	}

}
