package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Item;
import model.User;

/**
 * Servlet implementation class ItemDeleteServlet
 */
@WebServlet("/ItemDeleteServlet")
public class ItemDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ItemDeleteServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッションタイムアウト
		HttpSession session = request.getSession(false);
		User u = (User) session.getAttribute("user");

		if (u == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		try {
			String[] deleteItemByCartList = request.getParameterValues("delete_item_id_list");

			//	カートリスト取得
			ArrayList<Item> cart = (ArrayList<Item>) session.getAttribute("cart");

			//	カート削除
			if (deleteItemByCartList != null) {
				// 削除対象の商品を削除
				for (String deleteItemId : deleteItemByCartList) {
					for (Item cartInItem : cart) {
						if (cartInItem.getId() == Integer.parseInt(deleteItemId)) {
							cart.remove(cartInItem);
							break;
						}
					}
				}
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/item_delete.jsp");
				dispatcher.forward(request, response);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
