package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.User;

/**
 * Servlet implementation class UserInfoFixCheckServlet
 */
@WebServlet("/UserInfoFixCheckServlet")
public class UserInfoFixCheckServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserInfoFixCheckServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
//	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 文字化け防止
		request.setCharacterEncoding("UTF-8");

		// セッションタイムアウト
		 HttpSession session = request.getSession(false);
		 User u = (User) session.getAttribute("user");

		 if (u == null) {
		 response.sendRedirect("LoginServlet");
		 return;
		 }

		//入力した内容をゲット
		String password = request.getParameter("password");
		String passwordCheck = request.getParameter("passwordCheck");
		String name = request.getParameter("name");

		User updateUI = new User();
		updateUI.setPassword(password);
		updateUI.setName(name);

		//	分岐1 パスワードが異なる場合
		if (!(password.equals(passwordCheck)) ) {
			request.setAttribute("errMsg","入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_info_fix.jsp");
			dispatcher.forward(request, response);
			return ;
		}

		//	分岐2 空白の場合
		if (password.isEmpty() || passwordCheck.isEmpty() || name.isEmpty()) {
			request.setAttribute("errMsg","入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_info_fix.jsp");
			dispatcher.forward(request, response);
			return ;
		}


		//入力した内容を次の画面へ引き継ぎ
		request.setAttribute("updateUI",updateUI);

		//jspへフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_info_fixCheck.jsp");
		dispatcher.forward(request,response);
	}

}
