package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ItemDao;
import model.Item;
import model.User;

/**
 * Servlet implementation class HomeServlet
 */
@WebServlet("/HomeServlet")
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public HomeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// セッションタイムアウト
		HttpSession session = request.getSession(false);
		User u=(User)session.getAttribute("user");

		if (u == null){
			response.sendRedirect("LoginServlet");
			return;
		}


		try {
			// ホーム画面に商品をランダムで4件表示
			ArrayList<Item> itemList = ItemDao.getRandItem(4);

			request.setAttribute("itemList", itemList);

			//	検索ワードのセッション
			String searchWord = (String)session.getAttribute("searchWord");


			//	閲覧履歴


			//	商品閲覧履歴のセッション呼び出し
			ArrayList<Item> searchHistory = (ArrayList<Item>) session.getAttribute("searchHistory");

			//	セッションに閲覧履歴がない場合閲覧履歴を作成
			if (searchHistory == null) {
				searchHistory = new ArrayList<Item>();
			}

			//	閲覧履歴が7つを超える場合、古いものから消去
			if(searchHistory.size() > 7) {
				searchHistory.remove(0);
				session.getAttribute("searchHistory");
			}

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/home.jsp");
			dispatcher.forward(request,response);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}
}
