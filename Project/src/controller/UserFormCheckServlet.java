package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserFormCheckServlet
 */
@WebServlet("/UserFormCheckServlet")
public class UserFormCheckServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserFormCheckServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_form_check.jsp");
		dispatcher.forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();

		try {

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordCheck = request.getParameter("passwordCheck");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");

		User user = new User();
		user.setLoginId(loginId);
		user.setPassword(password);
		user.setName(name);
		user.setBirthDate(birthDate);


		//	分岐1 空白の場合
		if (loginId.isEmpty() || password.isEmpty() || passwordCheck.isEmpty() || name.isEmpty() || birthDate.isEmpty()) {
			request.setAttribute("errMsg","入力された内容は正しくありません");
			session.setAttribute("userForm", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_form.jsp");
			dispatcher.forward(request, response);
			return ;
		}

		//	分岐2 パスワードが異なる場合
		if (!(password.equals(passwordCheck)) ) {
			request.setAttribute("errMsg","入力された内容は正しくありません");
			session.setAttribute("userForm", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_form.jsp");
			dispatcher.forward(request, response);
			return ;
		}

		UserDao userForm =new UserDao();
		User ufc = userForm.UserFormIdCheck(loginId);

		//	分岐3 登録済みのログインIDの場合
		if (ufc!=null) {
			request.setAttribute("errMsg","既に利用されているIDです。");
			session.setAttribute("userForm", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_form.jsp");
			dispatcher.forward(request, response);
			return ;
		}

		request.setAttribute("userForm", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_form_check.jsp");
		dispatcher.forward(request,response);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
