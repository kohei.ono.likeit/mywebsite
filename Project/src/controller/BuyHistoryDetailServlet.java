package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BuyDao;
import dao.BuyDetailDao;
import model.Buy;
import model.Item;
import model.User;

/**
 * Servlet implementation class BuyHistoryDetailServlet
 */
@WebServlet("/BuyHistoryDetailServlet")
public class BuyHistoryDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BuyHistoryDetailServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// セッションタイムアウト
		HttpSession session = request.getSession(false);
		User u = (User) session.getAttribute("user");

		if (u == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		try {
			// 追加実装箇所
			String id = request.getParameter("id");

			BuyDao buyDao = new BuyDao();
			Buy buy = buyDao.getBuyHistoryByBuyId(id);
			request.setAttribute("buy", buy);

			// 購入詳細 商品名+配送名
			ArrayList<Item> buyItemList = BuyDetailDao.getItemDataListByBuyId(id);

			request.setAttribute("buyItemList", buyItemList);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buy_history_detail.jsp");
			dispatcher.forward(request, response);

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
