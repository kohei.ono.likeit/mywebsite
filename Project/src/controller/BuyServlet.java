package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DeliveryMethodDao;
import model.DeliveryMethod;
import model.Item;
import model.User;

/**
 * Servlet implementation class BuyServlet
 */
@WebServlet("/BuyServlet")
public class BuyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// セッションタイムアウト
		HttpSession session = request.getSession(false);
		User u=(User)session.getAttribute("user");

		if (u == null){
		response.sendRedirect("LoginServlet");
		return;
		}

		try {
			//	セッションのカート情報取得
			ArrayList<Item> cart = (ArrayList<Item>) session.getAttribute("cart");


			if (cart.size() == 0) {
				request.setAttribute("cartActionMessage", "購入する商品がありません");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/cart_list.jsp");
				dispatcher.forward(request,response);
			} else {
			// 配送方法をDBから取得
			ArrayList<DeliveryMethod> dMList = DeliveryMethodDao.getAllDeliveryMethod();
			request.setAttribute("dMList", dMList);
			}

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buy.jsp");
			dispatcher.forward(request,response);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
