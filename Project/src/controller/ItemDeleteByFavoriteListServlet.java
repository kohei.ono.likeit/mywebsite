package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Item;
import model.User;

/**
 * Servlet implementation class ItemDeleteByFavoriteListServlet
 */
@WebServlet("/ItemDeleteByFavoriteListServlet")
public class ItemDeleteByFavoriteListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ItemDeleteByFavoriteListServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// セッションタイムアウト
		HttpSession session = request.getSession(false);
		User u = (User) session.getAttribute("user");

		if (u == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		try {
			String[] deleteItemByFavoriteList = request.getParameterValues("delete_item_id_list");

			// お気に入りリスト取得
			ArrayList<Item> favorite = (ArrayList<Item>) session.getAttribute("favorite");

			// お気に入り削除
			if (deleteItemByFavoriteList != null) {
				// 削除対象の商品を削除
				for (String deleteItemId : deleteItemByFavoriteList) {
					for (Item favoriteInItem : favorite) {
						if (favoriteInItem.getId() == Integer.parseInt(deleteItemId)) {
							favorite.remove(favoriteInItem);
							break;
						}
					}
				}
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/item_delete.jsp");
				dispatcher.forward(request, response);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
