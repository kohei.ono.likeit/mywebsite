<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>購入完了</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome-animation/0.0.10/font-awesome-animation.css"
	type="text/css" media="all" />
</head>

<body>
	<div class="wrapper m-2">
		<!--        全画面共通　ヘッダー-->
		<header class="header" style="background-color: cornsilk;">
			<div class="row p-3">
				<div class="col-sm-2" style="text-align: center;">
					<a href="HomeServlet"> <i class="fas fa-home fa-2x fa-fw"></i>
					</a>
				</div>
				<div class="col-sm-3" style="text-align: end; margin: 0 0 0 auto;">
					<p>ようこそ ${user.name}さん</p>
				</div>
				<div class="col-sm-3" style="text-align: end;">
					<a href="UserServlet"> <i class="fas fa-user fa-2x fa-fw"></i>
					</a> <a href="CartListServlet"> <i
						class="fas fa-shopping-cart fa-2x fa-fw"></i>
					</a> <a href="FavoriteListServlet"> <i
						class="fas fa-heart fa-2x fa-fw"></i>
					</a> <a href="LogoutServlet"> <i
						class="fas fa-sign-out-alt fa-2x fa-fw"></i>
					</a>
				</div>
			</div>
		</header>

		<div class="title_box">
			<div class="row justify-content-center mt-5">
				<h2 class="title" style="font-weight: bold;">購入完了</h2>
			</div>
			<div class="row justify-content-center mt-5">
				<h4>ご注文ありがとうございます！</h4>
			</div>

			<div class="content_box">
				<p class="border mt-5" style="border: 1px solid #e7e7e7;"></p>
			</div>
            <h4 class="row justify-content-center mt-5 buy_detail" style="text-align: center; font-weight: bold;">
                <div class="col-sm-4">購入日時</div>
                <div class="col-sm-4">配送方法</div>
                <div class="col-sm-4">合計金額</div>
            </h4>
			<h5 class="row justify-content-center mt-5 buy_detail" style="text-align: center;">
                <div class="col-sm-4">${buyResult.formatDate}</div>
                <div class="col-sm-4">${buyResult.deliveryMethodName}</div>
                <div class="col-sm-4">￥${buyResult.formatTotalPrice}円</div>
			</h5>
		</div>

		<div class="content_box">
			<p class="border mt-5" style="border: 1px solid #e7e7e7;"></p>
		</div>
		<div class="row justify-content-center mt-5">
			<h4 class="check_alert" style="font-weight: bold;">購入した商品一覧</h4>
		</div>

		<div class="buy_list">
			<c:forEach var="BRL" items="${buyResultList}">
				<div class="row item_list mt-5">
					<div class="item col-sm-7"
						style="display: flex; width: 200px; align-items: center; justify-content: center;">
						<img src="img/${BRL.fileName}"
							style="width: 150px; height: 200px;">
					</div>
					<div class="item_card col-sm-5">
						<h4>${BRL.name}</h4>
						<p class="liner"
							style="border: 1px solid #e7e7e7; line-height: 20px;"></p>
						<h5>￥${BRL.price}円</h5>
					</div>
				</div>
				<div class="border_box mt-5">
					<p class="liner"
						style="border: 1px solid #e7e7e7; line-height: 20px;"></p>
				</div>
			</c:forEach>
			<div class="row item_list mt-5">
				<div class="space_card col-sm-7"
					style="display: flex; width: 200px; center; align-items: center; justify-content: center;">
					<p class="liner"
						style="border: 1px solid #e7e7e7; line-height: 20px;"></p>
				</div>
				<div class="delivery_card col-sm-5">
					<h4>${buyResult.deliveryMethodName}</h4>
					<p class="liner"
						style="border: 1px solid #e7e7e7; line-height: 20px;"></p>
					<h5>￥${buyResult.deliveryMethodPrice}円</h5>
				</div>
			</div>
		</div>
		<p class="liner mt-5"
			style="border: 1px solid #e7e7e7; line-height: 20px;"></p>
		<div class="row button_list justify-content-center mt-5"
			style="text-align: center;">
			<div class="button col-sm-3">
				<a class="btn btn-secondary" style="width: 200px;"
					href="BuyHistoryServlet">購入履歴はこちら</a>
			</div>
			<div class="button col-sm-3">
				<a class="btn btn-info" style="width: 200px;" href="HomeServlet">ホームへ</a>
			</div>
		</div>
		<p class="border mt-5" style="border: 1px solid #e7e7e7;"></p>

		<footer class="footer" style="background-color: cornsilk;">
			<div class="pt-3">
				<div class="home_button" style="text-align: center;">
					<a href="HomeServlet"> <i class="fas fa-home fa-2x fa-fw"></i>
					</a>
				</div>
				<div class="site_info mt-3" style="text-align: center;">
					<p>漫画のショッピングサイト Copyright ©2020</p>
				</div>
			</div>
		</footer>
</body>

</html>
