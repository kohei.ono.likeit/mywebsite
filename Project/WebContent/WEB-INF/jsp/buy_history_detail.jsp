<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>購入履歴詳細</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome-animation/0.0.10/font-awesome-animation.css"
	type="text/css" media="all" />
</head>

<body>
	<div class="wrapper m-2">
		<!--        全画面共通　ヘッダー-->
		<header class="header" style="background-color: cornsilk;">
			<div class="row p-3">
				<div class="col-sm-2" style="text-align: center;">
					<a href="HomeServlet"> <i class="fas fa-home fa-2x fa-fw"></i>
					</a>
				</div>
				<div class="col-sm-3" style="text-align: end; margin: 0 0 0 auto;">
					<p>ようこそ ${user.name}さん</p>
				</div>
				<div class="col-sm-3" style="text-align: end;">
					<a href="UserServlet"> <i class="fas fa-user fa-2x fa-fw"></i>
					</a> <a href="CartListServlet"> <i
						class="fas fa-shopping-cart fa-2x fa-fw"></i>
					</a> <a href="FavoriteListServlet"> <i
						class="fas fa-heart fa-2x fa-fw"></i>
					</a> <a href="LogoutServlet"> <i
						class="fas fa-sign-out-alt fa-2x fa-fw"></i>
					</a>
				</div>
			</div>
		</header>

		<div class="title_box">
			<div class="row justify-content-center mt-5">
				<h2 class="order_history" style="font-weight: bold;">購入履歴詳細</h2>
			</div>
		</div>
		<div class="content_box">
			<p class="border mt-5" style="border: 1px solid #e7e7e7;"></p>
		</div>
		<!--        購入履歴の詳細情報-->
		<div class="order_list">
			<h4 class="row justify-content-center mt-5 buy_detail"
				style="text-align: center; font-weight: bold;">
				<div class="col-sm-4">購入日時</div>
				<div class="col-sm-4">配送方法</div>
				<div class="col-sm-4">合計金額</div>
			</h4>
			<h5 class="row justify-content-center mt-5 buy_detail"
				style="text-align: center;">
				<div class="col-sm-4">${buy.formatDate}</div>
				<div class="col-sm-4">${buy.deliveryMethodName}</div>
				<div class="col-sm-4">￥${buy.formatTotalPrice}円</div>
			</h5>
		</div>
		<div class="content_box">
			<p class="border mt-5" style="border: 1px solid #e7e7e7;"></p>
		</div>

		<!--        商品の詳細情報-->
		<div class="title_box">
			<div class="row justify-content-center mt-5">
				<h2 class="order_history" style="font-weight: bold;">内訳</h2>
			</div>
		</div>
		<div class="content_box">
			<p class="border mt-5" style="border: 1px solid #e7e7e7;"></p>
		</div>

		<div class="buy_list">
			<c:forEach var="buyHistory" items="${buyItemList}">
				<div class="row item_list mt-5">
					<div class="item col-sm-7"
						style="display: flex; width: 200px; align-items: center; justify-content: center;">
						<img src="img/${buyHistory.fileName}"
							style="width: 150px; height: 200px;">
					</div>
					<div class="item_card col-sm-5">
						<h4>${buyHistory.name}</h4>
						<p class="liner"
							style="border: 1px solid #e7e7e7; line-height: 20px;"></p>
						<h5>￥${buyHistory.price}円</h5>
					</div>
				</div>
				<div class="border_box mt-5">
					<p class="liner"
						style="border: 1px solid #e7e7e7; line-height: 20px;"></p>
				</div>
			</c:forEach>
			<div class="row item_card mt-5">
				<div class="space_card col-sm-7"
					style="display: flex; width: 200px; center; align-items: center; justify-content: center;">
					<p class="liner"
						style="border: 1px solid #e7e7e7; line-height: 20px;"></p>
				</div>
				<div class="delivery_card col-sm-5">
					<h4>${buy.deliveryMethodName}</h4>
					<p class="liner"
						style="border: 1px solid #e7e7e7; line-height: 20px;"></p>
					<h5>￥${buy.deliveryMethodPrice}円</h5>
				</div>
			</div>
			<p class="border mt-5" style="border: 1px solid #e7e7e7;"></p>

			<div class="row button_list justify-content-center mt-5">
				<div class="button">
					<a class="btn btn-secondary" style="width: 200px; color: #fff;"
						href="BuyHistoryServlet">戻る</a>
				</div>
			</div>
		</div>
		<p class="border mt-5" style="border: 1px solid #e7e7e7;"></p>

		<footer class="footer" style="background-color: cornsilk;">
			<div class="pt-3">
				<div class="home_button" style="text-align: center;">
					<a href="HomeServlet"> <i class="fas fa-home fa-2x fa-fw"></i>
					</a>
				</div>
				<div class="site_info mt-3" style="text-align: center;">
					<p>漫画のショッピングサイト Copyright ©2020</p>
				</div>
			</div>
		</footer>
	</div>
</body>

</html>
