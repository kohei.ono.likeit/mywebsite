<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>購入履歴</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome-animation/0.0.10/font-awesome-animation.css"
	type="text/css" media="all" />
</head>

<body>
	<div class="wrapper m-2">
		<!--        全画面共通　ヘッダー-->
		<header class="header" style="background-color: cornsilk;">
			<div class="row p-3">
				<div class="col-sm-2" style="text-align: center;">
					<a href="HomeServlet"> <i class="fas fa-home fa-2x fa-fw"></i>
					</a>
				</div>
				<div class="col-sm-3" style="text-align: end; margin: 0 0 0 auto;">
					<p>ようこそ ${user.name}さん</p>
				</div>
				<div class="col-sm-3" style="text-align: end;">
					<a href="UserServlet"> <i class="fas fa-user fa-2x fa-fw"></i>
					</a> <a href="CartListServlet"> <i
						class="fas fa-shopping-cart fa-2x fa-fw"></i>
					</a> <a href="FavoriteListServlet"> <i
						class="fas fa-heart fa-2x fa-fw"></i>
					</a> <a href="LogoutServlet"> <i
						class="fas fa-sign-out-alt fa-2x fa-fw"></i>
					</a>
				</div>
			</div>
		</header>

		<div class="title_box">
			<div class="row justify-content-center mt-5">
				<h2 class="title" style="font-weight: bold;">購入履歴</h2>
			</div>
		</div>
		<div class="content_box">
			<p class="border mt-5" style="border: 1px solid #e7e7e7;"></p>
		</div>
		<div class="order_list">
			<div class="title_holder mt-5">
				<div class="row buy_date">
					<div class="buy_date col-sm-3"
						style="display: inline-block; text-align: center;">
						<h4>購入日時</h4>
					</div>
					<div class="delivery_method col-sm-3"
						style="display: inline-block; text-align: center;">
						<h4>配送方法</h4>
					</div>
					<div class="total_price col-sm-3"
						style="display: inline-block; text-align: center;">
						<h4>合計金額</h4>
					</div>
				</div>

				<c:forEach var="history" items="${buyHistory}">
					<div class="row mt-5 buy_date_list">
						<div class="buy_date col-sm-3"
							style="display: inline-block; text-align: center;">
							<h4>${history.formatDate}</h4>
						</div>
						<div class="delivery_method col-sm-3"
							style="display: inline-block; text-align: center;">
							<h4>${history.deliveryMethodName}</h4>
						</div>
						<div class="total_price col-sm-3"
							style="display: inline-block; text-align: center;">
							<h4>￥${history.formatTotalPrice}円</h4>
						</div>
						<div class="button col-sm-3"
							style="display: flex; justify-content: center; align-items: center;">
							<a href="BuyHistoryDetailServlet?id=${history.id}"
								class="btn btn-info" style="width: 100px; color: #fff;">詳細</a>
						</div>
					</div>
					<div class="mt-5 content_box">
						<p class="border" style="border: 1px solid #e7e7e7;"></p>
					</div>
				</c:forEach>
			</div>
		</div>

		<div class="row button_list justify-content-center mt-5">
			<div class="button">
				<a class="btn btn-secondary" style="width: 200px; color: #fff;"
					href="UserServlet">ユーザー情報へ</a>
			</div>
		</div>

		<p class="border mt-5" style="border: 1px solid #e7e7e7;"></p>

		<footer class="footer" style="background-color: cornsilk;">
			<div class="pt-3">
				<div class="home_button" style="text-align: center;">
					<a href="HomeServlet"> <i class="fas fa-home fa-2x fa-fw"></i>
					</a>
				</div>
				<div class="site_info mt-3" style="text-align: center;">
					<p>漫画のショッピングサイト Copyright ©2020</p>
				</div>
			</div>
		</footer>
	</div>
</body>

</html>
