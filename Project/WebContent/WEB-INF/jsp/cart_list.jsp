<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>カートリスト</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome-animation/0.0.10/font-awesome-animation.css"
	type="text/css" media="all" />
</head>

<body>
	<div class="wrapper m-2">
		<!--        全画面共通　ヘッダー-->
		<header class="header" style="background-color: cornsilk;">
			<div class="row p-3">
				<div class="col-sm-2" style="text-align: center;">
					<a href="HomeServlet"> <i class="fas fa-home fa-2x fa-fw"></i>
					</a>
				</div>
				<div class="col-sm-3" style="text-align: end; margin: 0 0 0 auto;">
					<p>ようこそ ${user.name}さん</p>
				</div>
				<div class="col-sm-3" style="text-align: end;">
					<a href="UserServlet"> <i class="fas fa-user fa-2x fa-fw"></i>
					</a> <a href="CartListServlet"> <i
						class="fas fa-shopping-cart fa-2x fa-fw"></i>
					</a> <a href="FavoriteListServlet"> <i
						class="fas fa-heart fa-2x fa-fw"></i>
					</a> <a href="LogoutServlet"> <i
						class="fas fa-sign-out-alt fa-2x fa-fw"></i>
					</a>
				</div>
			</div>
		</header>


		<!--    カート　全件表示-->

		<div class="title_box">
			<div class="row justify-content-center cart_title mt-5">
				<h2 class="title" style="font-weight: bold;">${user.name}さんのカートリスト</h2>
			</div>
			<div class="content_box">
				<p class="border mt-5" style="border: 1px solid #e7e7e7;"></p>
			</div>
		</div>

		<div class="row" style="display: block; text-align: center;">${cartActionMessage}</div>
		<div class="row comic_list" style="margin: 0 auto;">
			<c:forEach var="item" items="${cart}">
				<div class="col-sm-3 mt-5">
					<div class="card col-sm-8">
						<div class="card-image col-sm-4">
							<a href="ItemDetailServlet?item_id=${item.id}"><img
								src="img/${item.fileName}" style="width: 250px; height: 300px;"></a>
						</div>
						<div class="card-content p-3">
							<span class="card-title"
								style="font-size: 18px; font-weight: bold;">${item.name}</span>
							<p>${item.price}円</p>
						</div>
						<div class="listup p-2">
							<form action=ItemDeleteServlet method=POST
								style="display: inline-block;">
								<input type="hidden" name="delete_item_id_list"
									value="${item.id}">
								<button class="btn btn-secondary mt-3 p-2" type="submit"
									name="delete_item_id_list">
									カートから削除<i class="far fa-trash-alt fa-2x fa-fw"
										style="width: 200px;"></i>
								</button>
							</form>
						</div>
					</div>
				</div>
			</c:forEach>
		</div>
		<c:if test="${cart.size() != 0}">
			<div class="row justify-content-center">
				<div class="buy_button mt-5">
					<a class="btn btn-info" style="width: 200px" href="BuyServlet">購入手続きへ</a>
				</div>
			</div>
		</c:if>
	</div>

	<p class="border mt-5" style="border: 1px solid #e7e7e7;"></p>

	<footer class="footer" style="background-color: cornsilk;">
		<div class="pt-3">
			<div class="home_button" style="text-align: center;">
				<a href="HomeServlet"> <i class="fas fa-home fa-2x fa-fw"></i>
				</a>
			</div>
			<div class="site_info mt-3" style="text-align: center;">
				<p>漫画のショッピングサイト Copyright ©2020</p>
			</div>
		</div>
	</footer>
	</div>
</body>
</html>
