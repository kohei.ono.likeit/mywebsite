<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>商品一覧</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome-animation/0.0.10/font-awesome-animation.css" type="text/css" media="all" />
</head>

<body>
    <div class="wrapper m-2">
        <!--        全画面共通　ヘッダー-->
        <header class="header" style="background-color: cornsilk;">
            <div class="row p-3">
                <div class="col-sm-2" style="text-align: center;">
                    <a href="HomeServlet">
                        <i class="fas fa-home fa-2x fa-fw"></i>
                    </a>
                </div>
                <div class="col-sm-3" style="text-align: end; margin: 0 0 0 auto;">
                    <p>ようこそ ${user.name}さん</p>
                </div>
                <div class="col-sm-3" style="text-align: end;">
                    <a href="UserServlet">
                        <i class="fas fa-user fa-2x fa-fw"></i>
                    </a>
                    <a href="CartListServlet">
                        <i class="fas fa-shopping-cart fa-2x fa-fw"></i>
                    </a>
                    <a href="FavoriteListServlet">
                        <i class="fas fa-heart fa-2x fa-fw"></i>
                    </a>
                    <a href="LogoutServlet">
                        <i class="fas fa-sign-out-alt fa-2x fa-fw"></i>
                    </a>
                </div>
            </div>
        </header>
        <div class="justify-content-center">
            <div class="row col-sm-10" style="margin: 0 auto;">
                <h2 class="title p-5" style="font-weight: bold;">${item.name}</h2>
            </div>
            <div class="content_box">
                <p class="border" style="border: 1px solid #e7e7e7;"></p>
            </div>
            <div class="comic_card row mt-5">
                <div class="comic_img col-sm-6">
                    <div class="item_card" style="text-align: center;">
                        <img src="img/${item.fileName}" style="width: 400px; height: 500px;">
                    </div>
                </div>
                <div class="comic_detail col-sm-6">
                    <h4>${item.name}</h4>
                    <p class="liner" style="border: 1px solid #e7e7e7; line-height: 20px;"></p>
                    <h5>￥${item.price}円</h5>
                    <p>${item.detail}</p>
                    <p class="liner" style="border: 1px solid #e7e7e7; line-height: 20px;"></p>
                    <div class="listup col-sm-12">
                    	<form action=FavoriteAddFinishServlet method=POST style="display: inline-block;">
                    		<input type="hidden" name="item_id" value="${item.id}">
							<button class="btn btn-info p-2" type="submit" name="item_id">
	                            お気に入りに追加<i class="fab fa-gratipay fa-2x fa-fw"></i>
	                        </button>
	                    </form>
	                    <form action=CartAddFinishServlet method=POST style="display: inline-block;">
							<input type="hidden" name="item_id" value="${item.id}">
	                        <button class="btn btn-warning ml-5 p-2" type="submit" name="item_id">
	                            カートに追加<i class="fas fa-cart-plus fa-2x fa-fw"></i>
	                        </button>
                    	</form>
                    </div>
                </div>
            </div>
        </div>
        <p class="border mt-5" style="border: 1px solid #e7e7e7;"></p>

        <footer class="footer" style="background-color: cornsilk;">
            <div class="pt-3">
                <div class="home_button" style="text-align: center;">
                    <a href="HomeServlet">
                        <i class="fas fa-home fa-2x fa-fw"></i>
                    </a>
                </div>
                <div class="site_info mt-3" style="text-align: center;">
                    <p>漫画のショッピングサイト　Copyright ©2020</p>
                </div>
            </div>
        </footer>
    </div>
</body>

</html>

