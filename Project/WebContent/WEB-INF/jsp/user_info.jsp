<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ユーザー情報</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome-animation/0.0.10/font-awesome-animation.css" type="text/css" media="all" />
</head>

<body>
    <div class="wrapper m-2">
        <!--        全画面共通　ヘッダー-->
        <header class="header" style="background-color: cornsilk;">
            <div class="row p-3">
                <div class="col-sm-2" style="text-align: center;">
                    <a href="HomeServlet">
                        <i class="fas fa-home fa-2x fa-fw"></i>
                    </a>
                </div>
                <div class="col-sm-3" style="text-align: end; margin: 0 0 0 auto;">
                    <p>ようこそ ${user.name}さん</p>
                </div>
                <div class="col-sm-3" style="text-align: end;">
                    <a href="UserServlet">
                        <i class="fas fa-user fa-2x fa-fw"></i>
                    </a>
                    <a href="CartListServlet">
                        <i class="fas fa-shopping-cart fa-2x fa-fw"></i>
                    </a>
                    <a href="FavoriteListServlet">
                        <i class="fas fa-heart fa-2x fa-fw"></i>
                    </a>
                    <a href="LogoutServlet">
                        <i class="fas fa-sign-out-alt fa-2x fa-fw"></i>
                    </a>
                </div>
            </div>
        </header>

        <div class="title" style="text-align: center;">
            <div class="row justify-content-center mt-5">
                <h2 class="title" style="font-weight: bold;">ユーザー情報</h2>
            </div>
            <div class="content_box">
                <p class="border mt-3" style="border: 1px solid #e7e7e7;"></p>
            </div>
            <div class="login_form mt-5 p-5" style="background-color: cornsilk; margin: 0 auto;">
                <!--    ログインID入力-->
                <div class="row justify-content-center p-2">
                    <div class="col-sm-2">
                        <P>ログインID</P>
                    </div>
                    <div class="col-sm-2">
	                    <p>${user.loginId}</p>
                    </div>
                </div>

                <!--    ユーザ名入力-->
                <div class="row justify-content-center p-2">
                    <div class="col-sm-2">
                        <P>ユーザ名</P>
                    </div>
                    <div class="col-sm-2">
	                    <p>${user.name}</p>
                    </div>
                </div>

                <!--    生年月日入力-->
                <div class="row justify-content-center p-2">
                    <div class="col-sm-2">
                        <P>生年月日</P>
                    </div>
                    <div class="col-sm-2">
	                    <p>${user.birthDate}</p>
                    </div>
                </div>

                <div class="row justify-content-center mt-5">
                    <div class="col-sm-3">
                        <a class="btn btn-secondary" style="width: 200px; color: #fff;" href="BuyHistoryServlet">購入履歴</a>
                    </div>
                    <div class="col-sm-3">
                        <a class="btn btn-info" style="width: 200px; color: #fff;" href="UserInfoFixServlet">修正</a>
                    </div>
                </div>
            </div>
        </div>
        <p class="border mt-5" style="border: 1px solid #e7e7e7;"></p>

        <footer class="footer" style="background-color: cornsilk;">
            <div class="pt-3">
                <div class="home_button" style="text-align: center;">
                    <a href="HomeServlet">
                        <i class="fas fa-home fa-2x fa-fw"></i>
                    </a>
                </div>
                <div class="site_info mt-3" style="text-align: center;">
                    <p>漫画のショッピングサイト　Copyright ©2020</p>
                </div>
            </div>
        </footer>
    </div>
</body>
</html>