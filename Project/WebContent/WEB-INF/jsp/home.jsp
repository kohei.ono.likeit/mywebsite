<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>ホーム</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome-animation/0.0.10/font-awesome-animation.css"
	type="text/css" media="all" />
</head>

<body>
	<div class="wrapper m-2">
		<!--        全画面共通　ヘッダー-->
		<header class="header" style="background-color: cornsilk;">
			<div class="row p-3">
				<div class="col-sm-2" style="text-align: center;">
					<a href="HomeServlet"> <i class="fas fa-home fa-2x fa-fw"></i>
					</a>
				</div>
				<div class="col-sm-3" style="text-align: end; margin: 0 0 0 auto;">
					<p>ようこそ ${user.name}さん</p>
				</div>
				<div class="col-sm-3" style="text-align: end;">
					<a href="UserServlet"> <i class="fas fa-user fa-2x fa-fw"></i>
					</a> <a href="CartListServlet"> <i
						class="fas fa-shopping-cart fa-2x fa-fw"></i>
					</a> <a href="FavoriteListServlet"> <i
						class="fas fa-heart fa-2x fa-fw"></i>
					</a> <a href="LogoutServlet"> <i
						class="fas fa-sign-out-alt fa-2x fa-fw"></i>
					</a>
				</div>
			</div>
		</header>

		<!--        検索フォーム-->
		<div class="row justify-content-center">
			<div class="col-sm-8 m-5">
				<form action=ItemListServlet method=POST>
					<i class="fas fa-search fa-2x fa-fw"></i> <input type="text"
						name="searchWord"
						style="width: 90%; border: none; border-bottom: solid;">
				</form>
			</div>
		</div>
		<div class="justify-content-center">
			<div class="p-5 col-sm-10" style="margin: 0 auto;">
				<h2 class="title" style="font-weight: bold;">あなたへのおすすめ</h2>
			</div>
			<p class="border" style="border: 1px solid #e7e7e7;"></p>
			<div class="row comic_list mt-5">
				<c:forEach var="item" items="${itemList}">
					<div class="col sm-12 m-3" style="display: flex;">
						<div class="card col-sm-8">
							<div class="card-image">
								<a href="ItemDetailServlet?item_id=${item.id}"><img
									src="img/${item.fileName}" style="width: 250px; height: 300px;"></a>
							</div>
							<div class="card-content col-sm-12 p-3">
								<span class="card-title" style="font-size: 18px; font-weight: bold;">${item.name}</span>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
		<div class="justify-content-center">
			<p class="border mt-5" style="border: 1px solid #e7e7e7;"></p>
			<div class="p-5 col-sm-10" style="margin: 0 auto;">
				<h2 class="title" style="font-weight: bold;">閲覧履歴</h2>
			</div>
			<div class="justify-content-center border_box">
				<p class="border" style="border: 1px solid #e7e7e7;"></p>
				<ul class="banner_list" style="list-style: none;">
					<c:forEach var="search" items="${searchHistory}">
						<a href="ItemDetailServlet?item_id=${search.id}">
							<li style="display: inline-block; margin: 50px;"><img
								src="img/${search.fileName}" style="width: 150px; height: 200px;"></li>
						</a>
					</c:forEach>
				</ul>
				<p class="border" style="border: 1px solid #e7e7e7;"></p>
			</div>
		</div>

		<footer class="footer" style="background-color: cornsilk;">
			<div class="pt-3">
				<div class="home_button" style="text-align: center;">
					<a href="HomeServlet"> <i class="fas fa-home fa-2x fa-fw"></i>
					</a>
				</div>
				<div class="site_info mt-3" style="text-align: center;">
					<p>漫画のショッピングサイト Copyright ©2020</p>
				</div>
			</div>
		</footer>
	</div>
</body>

</html>
